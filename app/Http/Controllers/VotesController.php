<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Console\Question\Question;

class VotesController extends Controller
{
    public function voteQuestion(Question $question, int $vote)
    {
        if (auth()->user()->hasVotesForQuestion($question)) {
            //update votes
            if (($vote == 1 && ! auth()->user()->hasVotesForQuestion($question)) ||
                ($vote == -1 && ! auth()->user()->hasVotesForQuestion($question))) {
                    $question->updateVote($vote);
            }
        } else {
            //create new vote for that question
            $question->vote($vote);
        }
        return redirect()->back();
    }
}
